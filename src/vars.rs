// Variables are unchangable by default 
pub fn run() {
    //use let to define a variable 
    let name = "Brad";
    // mut allows the variable to be changed
    let mut age = 16;
    println!("My name is {} and i am {}", name, age);
    // this assigns a new thing to the variable. 
    age = 85;
    println!("Now {} is {} (he got old pretty quick", name, age);

    // Const can also be used to make immutable variables, with const
    // however you must define a datatype
    const ID: i32 = 001;
    println!("ID: {}", ID);

    // assign multiple variables at the same time
    let ( my_name, my_age ) = ( "Arkaitz", 15 );
    println!("{} is {}", my_name, my_age);

}
