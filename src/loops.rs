// 1:11:53
pub fn run() {
    let mut count: i64 = 0;
    
    //Infinite loop
    loop {
        count += 1;
        println!("Number: {}", count);
        
        if count == 8800 {
            break;
        }
    }

    count = 0;
    // While loop (FizzBuzz)
    while count <= 100 {
        if count % 15 == 0 {
            println!("fizzbuzz");
        }
        else if count % 3 == 0 {
            println!("fizz");
        }
        else if count % 5 == 0 {
            println!("buzz");
        }
        else {
            println!("{}", count);
        }
    }
}
