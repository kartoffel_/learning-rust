// tuples are a group of values which may have different types
// max os 12
pub fn run() {
    // sets the datatypes for the tuple
    let person: (&str, &str, i8) = ("Arkaitz", "Prospect Hill", 15);

    // access parts of the tuple with tuple.placenumber (starts at 0)
    println!("{} is from {} and is {}", person.0, person.1, person.2);

}
