/*
 Primitive Types
 Integers: u8, i8, u16, i16, u32, i32, u64, i64, u128, i128 
 u means no negative numbers, i can have either, larger the number == more bits used in memory
 Floats: f32, f64
 Boolean (true/false)
 Characters (char)
 Tuples
 Arrays
 */
// Rust is a statically typed language, this means it needs to know the times of all
// variables at compile time, but the compiler is not an idiot and can usually infer what
// type you want based on the value and how its used. 
pub fn run() {
    // This is an "i32" by default
    let x = 1;

    // it figures out that its a float and puts it as f64 by default
    let y = 2.5;

    //to explicitely define the type
    let z: i64 = 5389538959385;

    // To find the maximum size of a 32 bit integer or other type of integer you can run
    println!("Max i32: {}", std::i32::MAX);
    // std calls rusts standard library of functions 
    // same thing for 64 bit Integers
    println!("Max i64: {}", std::i64::MAX);
    // same thing again for 128 bit Integers
    println!("Max i128: {}", std::i128::MAX);

    // use lowercase for bools, it infered its a boolean but you can state it with : bool
    let is_active = true;

    // this will to the equation and see whether the answer is true or false, again you 
    // can state it explicitely
    let is_greater = 10 > 11;

    // '' specifies a single unicode character, you can use this for glyphs using \u{}
    let a1 = '\u{1f600}';

    println!("{:?}", (x, y, z, is_active, is_greater, a1));


}
