// Arrays are fixed lists where all elements must be the same datatype

// using this means that we dont need to use std::mem::size_of_val all the time,
// we can just use mem::size_of_val
use std::mem;

pub fn run() {
    //defines datatype and length in the [], [datatype; length], if the length or datatype is 
    //wrong it will error
    let mut numbers: [i32; 5] = [1, 2, 3, 4, 5];
    // you can use the debug trait to print the whole array
    println!("{:?}",numbers);

    // To reasign a value, cant add but you can reasign
    numbers[2] = 20;

    // To print certain parts of the array use array[place]
    println!("{}", numbers[2]);

    // To get the length
    println!("Array Length: {}", numbers.len());
    
    // To see the memory that it uses
    println!("This array occupies {} bytes", mem::size_of_val(&numbers));

    // To get a slice of the array
    // This lets you assign spots from an array, .. gives a range
    let slice = &numbers[0..4];

    println!("{:?}", slice);

}
