pub fn run() {
    // This is how to print to the console
    println!("Hello from the print.rs file");
    
    // {} are used to put in variables or numbers, basically anything that isnt a string is printed with {} and outside of the brackets use the inputs seperated by ,
    println!("Integers printed with: {} {}",1,2); 

    // You can specify where to put answers using intigers in the {}, 0 will pull the first value (in this case Brad) and it goes up from there
    // You can also break it over multiple lines 
    println!("{0} is from {1} and {0} likes {2}",
             "Arkaitz", "Prospect Hill", "Tech Arts"
             );
    // Named arguments, you can also specify names (like mini variables)
    println!("{name} like to play {activity}", name="John", activity =  "footy");

    // Placeholder traits
    // These give us the traits of the numbers, for example this prints out the value of 10 in
    // binary, hexidecimal and octal
    println!("Binary: {:b} Hex: {:x} Octal: {:o}", 10, 10, 10);
    
    // Placeholder for a debug trait, just prints whatever (i think)
    println!("{:?}", (12, true, "hi"));

    // You can do basic math 
    println!("10 + 10 = {}", 10 + 10);
}
