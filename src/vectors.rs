//Vectors are resizable arrays
// continue at 0.57.32

use std::mem;

pub fn run() {

    let mut numbers: Vec<i32> = vec![1, 2, 3, 4, 5];
    // you can use the debug trait to print the whole vectors
    println!("{:?}",numbers);

    // To reasign a value 
    numbers[2] = 20;

    // Add on to vector
    numbers.push(9);

    println!("The new vector is {:?}", numbers);

    // Gets rid of the last value (pops it off)
    numbers.pop();

    // Loop through vector values and prints out each one. 
    for x in numbers.iter() {
        println!("Number: {}", x);
    }

    // iter_mut allows the vector to be edited. 
    for x in numbers.iter_mut() {
        // This assigns the value to itself *2, im not sure what the *x does. 
        *x *= 2;
    }
    println!("Numbers Vec: {:?}", numbers);

    println!("The new new vector is {:?}", numbers);

    // To print certain parts of the vector use vector[place]
    println!("{}", numbers[2]);

    // To get the length
    println!("Vector Length: {}", numbers.len());
    
    // To see the memory that it uses
    println!("This vector occupies {} bytes", mem::size_of_val(&numbers));

    // To get a slice of the array
    // This lets you assign spots from a vector, .. gives a range
    let mut slice: &[i32] = &numbers[0..4];
    let num = &numbers[1] + 9;
 
    println!("The result of {} and {} is {}", numbers[1], 9, num);

}
