// Primitive str, these strings are immutable and have a fixed length
// String = Growable, this can be modified
pub fn run() {
    // this is the primitive string
    let hello = "Hello";

    // this string is the second type, 
    let mut string = String::from("Hello ");

    // Get length, this works for either type of string, DOES NOT START AT 0
    println!("Length: {}", hello.len());

    // this adds W onto the end of the string variable, only works on the complex String type 
    // This is the char datatype so single character and can be unicode
    string.push('W');

    // This adds a string onto the end, no unicode and can be multiple characters 
    string.push_str("orld!");

    // this lists the capacity of the string (how many bytes it can store)
    println!("Capacity: {}", string.capacity());

    // checks if the string is empty
    println!("Is Empty: {}", string.is_empty());

    // Checks whether the string contains a substring
    println!("Contains 'World': {}", string.contains("World"));

    // Replace, replaces the strings with a new string
    println!("Replace: {} \n You are a bold one", string.replace("World", "There"));

    // this seperates the string by its whitespaces and then prints them out on new lines
    for str in string.split_whitespace() {
        println!("{}", str);
    }

    // This creates a string with a stated capacity
    let mut s = String::with_capacity(10);
    s.push('a');
    s.push_str("b");
    println!("{}", s);

    // checks if 2 items are the same, errors if false
    assert_eq!(2, s.len());
    assert_eq!(10, s.capacity());


}
